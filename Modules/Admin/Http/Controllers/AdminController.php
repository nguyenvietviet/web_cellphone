<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Product;
use App\Models\Rating;
use App\Models\Contact;
use App\Models\Transaction;
use App\Models\Article;
use App\User;
use DB;

class AdminController extends Controller
{

    public function index()
    {

        $ratings = Rating::with('user:id,name','product:id,pro_name')->limit(5)->get();

        $contacts = Contact::limit(5)->get();

        //doanh thu ngày
        $moneyDay = Transaction::whereDay('updated_at',date('d'))
        ->where('tr_status',Transaction::STATUS_DONE)
        ->sum('tr_total');

        //doanh thu tháng
        $moneyMonth = Transaction::whereMonth('updated_at',date('m'))
        ->where('tr_status',Transaction::STATUS_DONE)
        ->sum('tr_total');

        //doanh thu tuần
        $moneyYear = Transaction::whereYear('updated_at',date('Y'))
        ->where('tr_status',Transaction::STATUS_DONE) 
        ->sum('tr_total');

        $dataMoney =[
            [
                "name" => "Doanh thu ngày",
                "y" => (int)$moneyDay
            ],
            
            [
                "name" => "Doanh thu tháng",
                "y" => (int)$moneyMonth
            ],
            [
                "name" => "Doanh thu năm",
                "y" => (int)$moneyYear
            ],

        ];

        //Danh sách đơn hàng mới 
        $transactionNews= Transaction::with('user:id,name')
        ->limit(5)->orderByDesc('id')->get();


        //Đếm Số lượng User và Bài viết 

        $users = User::all();
        $article = Article::all();
        $product = Product::all();
        $ratings = Rating::all();

        $viewData =[
            'ratings' => $ratings,
            'contacts' => $contacts,
            'moneyDay' => $moneyDay,
            'moneyMonth' => $moneyMonth,
            'moneyYear' => $moneyYear,
            'users' => $users,
            'article' => $article,
            'product' => $product,
            'dataMoney' => json_encode($dataMoney),
            'transactionNews' => $transactionNews
        ];

        return view('admin::index',$viewData);
    }

    
}

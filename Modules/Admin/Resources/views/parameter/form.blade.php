<form action="" method="post" enctype="multipart/form-data">
   @csrf
   <div class="row">
      <div class="offset-2 col-md-8">
         
         <div class="form-group">
            <label for="name">Nội dung:</label>
            <textarea name="para_name" class="form-control" id="a_content" cols="30" rows="3" placeholder="Nội dung" value="">{{ old('para_name',isset($para->para_name) ? $para->para_name : '')}}</textarea>
            @if($errors->has('para_name'))
            <span class="error-text">
            {{$errors->first('para_name')}}
            </span>
            @endif
         </div>
        
         <button type="submit" class="btn btn-primary btn-sm">
   <i class="fa fa-dot-circle-o"></i> Lưu thông tin
   </button>
      </div>
      
   </div>
   
</form>
@section('js')
<script>
        CKEDITOR.replace( 'a_content', {
            language:'vi',
            filebrowserBrowseUrl: 'http://localhost/Web_Cellphone/public/ckfinder/ckfinder.html',
            filebrowserUploadUrl: 'http://localhost/Web_Cellphone/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        } );
    </script>
@endsection
@extends('admin::layouts.master')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.create.page_static')}}">PageStatic/</a><a href="{{route('admin.get.list.page_static')}}">Danh mục</a></h1>
         </div>
      </div>
   </div>
</div>

<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Quản lý bài viết <a href="{{route('admin.get.create.page_static')}}" class="pull-right badge badge-primary"><i class="fas fa-plus-circle"></i> Thêm mới</a></strong>
            </div>
            <div class="card-body">
               <table class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th style="width: 20%;">Tên bài viết</th>
                        <th>Thời gian tạo</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($pages))
                     @foreach($pages as $page)
                     <tr>
                        <td>{{$page -> id}}</td>
                        <td>{{$page -> ps_name}}
                        <td>{{$page->created_at}}</td>
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.page_static',$page->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a onclick="return confirm('Bạn có muốn xóa')" class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href="{{route('deletePage',$page->id)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection
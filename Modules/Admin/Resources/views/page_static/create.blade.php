@extends('admin::layouts.master')
@section('content')
{{-- <div class="card-header"> --}}
<div class="breadcrumbs">
   <div class="col-sm-8">
      <div class="page-header float-left">
         <div class="page-title menu">
            
            <h1 style="color: blue;"><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.list.page_static')}}">Bài viết/</a><a href="{{route('admin.get.create.page_static')}}">Thêm mới</a></h1>
         
         </div>
      </div>
   </div>
   {{-- <li><a href="">Trang chủ</a></li>
   <li><a href="">Danh mục</a></li>
   <li><a href="">Thêm mới</a></li> --}}

</div>
{{-- </div> --}}
<div class="card">
   <div class="card-header">

      <strong>Thêm mới</strong> 
   </div>
   <div class="card-body card-block">
      @include("admin::page_static.form")
      {{-- <form action="" method="post">
        @csrf
         <div class="form-group">
            <label for="name">Tên danh mục</label>
            <input type="text" class="form-control" placeholder="Tên danh mục" value="{{ old('name')}}" name="name">
                  @if($errors->has('name'))
              <span class="error-text">
                  {{$errors->first('name')}}
              </span>
                @endif
         </div>
         <div class="form-group">
            <label for="name">Icon:</label>
            <input type="text" class="form-control" placeholder="fa fa-icon" value="{{ old('icon')}}" name="icon">
             @if($errors->has('icon'))
              <span class="error-text">
                  {{$errors->first('icon')}}
              </span>
                @endif
         </div>
         <div class="form-group">
            <label for="name">Meta title:</label>
            <input type="text" class="form-control" placeholder="Meta title" value="{{ old('c_title_seo')}}" name="c_title_seo">
            
         </div>
         <div class="form-group">
            <label for="name">Meta desciption:</label>
            <input type="text" class="form-control" placeholder="Meta desciption" value="{{ old('c_description_seo')}}" name="c_description_seo">
         </div>
         <div class="checkbox">
            <label><input type="checkbox" name="hot"> Nổi bật</label>
         </div>
         <button type="submit" class="btn btn-primary btn-sm">
         <i class="fa fa-dot-circle-o"></i> Thêm mới
         </button>
         <button type="reset" class="btn btn-danger btn-sm">
         <i class="fa fa-ban"></i> Reset
         </button>
      </form> --}}
   </div>
   {{-- 
   <div class="card-footer">
   </div>
</div>
--}}
@endsection
@extends('admin::layouts.master')
@section('content')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<style>
    .highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#container {
    height: 400px;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
</style>

<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="">Tổng Quan</a></h1>
                    </div>
                </div>
            </div>
        </div>

<div class="content mt-3">

<div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

{{-- {{count((is_countable($users)?$users:['id']))}} --}}
            <h4 class="mb-0">
                <span class="count">{{count($users)}}</span>
            </h4>
  
            <p class="text-light">Thành viên</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>
<div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

            <h4 class="mb-0">
                <span class="count">{{count($article)}}</span>
            </h4>
            <p class="text-light">Bài viết</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>


<div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

            <h4 class="mb-0">
                <span class="count">{{count($product)}}</span>
            </h4>
            <p class="text-light">Sản phẩm</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>


<div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

            <h4 class="mb-0">
                <span class="count">{{count($ratings)}}</span>
            </h4>
            <p class="text-light">Đánh giá</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-6">
      <figure class="highcharts-figure">
      <div id="container"></div>
    {{-- <p class="highcharts-description">
        Chart showing browser market shares. Clicking on individual columns
        brings up more detailed data. This chart makes use of the drilldown
        feature in Highcharts to easily switch between datasets.
    </p> --}}
</figure>
  </div>
  <div class="col-md-6" style="margin-top: 1.3%;">
    <div class="card-header">
                            <strong class="card-title">Danh sách đơn hàng mới </strong>
                        </div>
    <table  class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Tên khách hàng</th>
                  {{--       <th>Địa chỉ</th> --}}
                        <th>Số ĐT</th>
                        <th>Tổng tiền</th>
                        <th>Trạng thái</th>
                        <th>Time</th>
                        {{-- <th>Thao tác</th> --}}
                     </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; ?>
                     @foreach($transactionNews as $transaction)
                     <tr>
                        <td>{{$i++}}</td>
                        <td>{{isset($transaction->user->name) ? $transaction->user->name : '[N\A]'}}</td>
                        {{-- <td>{{$transaction->tr_address}}</td> --}}
                        <td>{{$transaction->tr_phone}}</td>
                        <td>{{number_format($transaction->tr_total,0,',','.')}}VNĐ</td>
                        <td>
                           @if($transaction->tr_status == 1)
                           <a href="#" class="badge badge-success">Đã xử lý</a>
                           @else
                           <a href="{{route('admin.get.active.transaction',$transaction->id)}}" class="badge badge-secondary">Chờ xử lý</a>
                           @endif
                        </td>
                        <td>{{$transaction->created_at->format('d-m-Y')}}</td>
                        {{-- <td>
                           <a class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href=""><i class="fas fa-trash-alt"></i> Xóa</a>
                           <button class="badge badge-info js_order_item" data-toggle="modal" data-target ="#myModalOrder" data-id="{{$transaction->id}}" style="padding:5px 10px;border:1px solid #eee;" data-href="{{route('admin.get.view.order',$transaction->id)}}"><i class="fas fa-eye"></i> Xem</button>
                        </td> --}}
                     </tr>
                     @endforeach
                     <?php $i++; ?>
                  </tbody>
               </table>
  </div>
</div>

</div>
   <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Danh sách liên hệ mới nhất</strong>
                        </div>
                        <div class="card-body">
                  <table id="" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tiêu đề</th>
                        <th>Họ tên</th>
                        <th>Email</th>
                        <th>Nội dung</th>
                        <th>Trạng thái</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; ?>
                      @if(isset($contacts))
                      @foreach($contacts as $contact)
                        <tr>
                          <td>{{$i++}}</td>
                          <td>{{$contact->c_title}}</td>
                          <td>{{$contact->c_name}}</td>
                          <td>{{$contact->c_email}}</td>
                          <td>{{$contact->c_content}}</td>
                          <th>
                               @if($contact->c_status == 1)
                               <a href="#" class="badge badge-success">Đã xử lý</a>
                               @else
                               <a href="#" class="badge badge-secondary">Chờ xử lý</a>
                               @endif
                          </th>
                        </tr>
                      @endforeach
                      @endif
                      <?php $i++ ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Danh sách đánh giá mới nhất</strong>
                        </div>
                        <div class="card-body">
                    <table id="" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tên thành viên</th>
                        <th>Sản phẩm</th>
                        <th>Rating</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; ?>
                     @if(isset($ratings))
                        @foreach($ratings as $rating)
                            <tr>
                        <td>{{$i++}}</td>
                        <td>{{$rating->user->name}}</td>
                        <td>{{$rating->product->pro_name}}</td>
                        <td>{{$rating->ra_number}}</td>
                     </tr>
                        @endforeach
                     @endif
                     <?php $i++ ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>
                
                </div>
            </div><!-- .animated -->
@endsection

@section('js')
  <script>

    let data = "{{$dataMoney}}";

    
    dataChart = JSON.parse(data.replace(/&quot;/g,'"'));
    // var dataChart = jQuery.parseJSON(data);
    console.log(dataChart)

    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Biểu đồ doanh thu trong Ngày và Tháng'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Biểu đồ'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}VNĐ'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "Browsers",
            colorByPoint: true,
            data: dataChart
          }
    ]
});
  </script>
@stop
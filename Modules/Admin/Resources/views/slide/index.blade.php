@extends('admin::layouts.master')
@section('content')
<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Slide</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active">Slide</a></li>
                            <li><a href="{{route('admin.get.list.slide')}}">Danh sách</a></li>
                            <li><a href="{{route('admin.get.create.slide')}}">Thêm</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

	<div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Table</strong>
                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Nội dung</th>
                        <th>Slide</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <?php $i=1 ?>
                    <tbody>
                      @foreach($slide as $sl)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$sl->ten}}</td>
                        <td>{!!$sl->noidung!!}</td>
                        <td>
                            <img width="300px" src="image_slide/{{$sl->hinh}}"/>
                        </td>
                        <td>
                          <a href="{{route('admin.get.edit.slide',$sl->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('deleteSl',$sl->id)}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                   <?php $i++ ?>
                        </div>
                    </div>
                </div>


                </div>
            </div>
@endsection
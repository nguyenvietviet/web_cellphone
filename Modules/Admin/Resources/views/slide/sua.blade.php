@extends('admin::layouts.master')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1>Slide</h1>
         </div>
      </div>
   </div>
   <div class="col-sm-8">
      <div class="page-header float-right">
         <div class="page-title">
            <ol class="breadcrumb text-right">
               <li class="active">Slide</a></li>
               <li><a href="{{route('admin.get.list.slide')}}">Danh sách</a></li>
               <li><a href="{{route('admin.get.create.slide')}}">Thêm</a></li>
            </ol>
         </div>
      </div>
   </div>
</div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Thêm Slide</strong>
            </div>
            <div class="card-body">
              
                <form action="admin/slide/sua/{{$slide->id}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      
                      <div class="form-group">
                          <label>Tên</label>
                          <input class="form-control" name="Ten" value="{{$slide->ten}}" placeholder="Nhập vào tiêu đề" />
                      </div>
          
                      <div class="form-group">
                          <label>Nội dung</label>
                          <textarea name="NoiDung" id="ckeditor" class="form-control ckeditor" rows="3">{{$slide->noidung}}</textarea>
                      </div>
                     
                      <div class="form-group">
                          <label>Hình ảnh</label>
                           <p>
                              <img width="400px" src="image_slide/{{$slide->hinh}}">
                           </p>
                           <input type="file" name="Hinh" class="form-control"/>
                      </div>
                  
                      <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                      <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
                  <form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
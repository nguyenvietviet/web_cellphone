@extends('admin::layouts.master')

@section('content')
 <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.list.rating')}}">Đánh giá</a></h1>
                    </div>
                </div>
            </div>
        </div>

	 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Quản lý đánh giá</strong>

                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tên thành viên</th>
                        <th>Sản phẩm</th>
                        <th>Nội dung</th>
                        <th>Rating</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                     @if(isset($ratings))
                        @foreach($ratings as $rating)
                            <tr>
                        <td>{{$rating -> id}}</td>
                        <td>{{$rating->user->name}}</td>
                        <td>{{$rating->product->pro_name}}</td>
                        <td>{{$rating -> ra_content}}</td>
                        <td>{{$rating -> ra_number}}</td>
                        <td>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('deleteR',$rating->id)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                        @endforeach
                     @endif
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
@endsection
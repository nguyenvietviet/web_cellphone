@extends('admin::layouts.master')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.create.article')}}">Bài viết/</a><a href="{{route('admin.get.list.article')}}">Danh mục</a></h1>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <form class="form-inline" action="" style="margin-bottom: 20px; margin-top: 10px;">
          <div class="form-group">       
            <input type="text" class="form-control" placeholder="Tên bài viết..." name="name" value="{{ \Request::get('name')}}">
          </div>
          
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
   </div>
</div>

<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Quản lý bài viết <a href="{{route('admin.get.create.article')}}" class="pull-right badge badge-primary"><i class="fas fa-plus-circle"></i> Thêm mới</a></strong>
            </div>
            <div class="card-body">
               <table id="bootstrap-data-table" class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th style="width: 20%;">Tên bài viết</th>
                        <th style="width: 150px;">Hình ảnh</th>
                        <th style="width: 300px;">Mô tả</th>
                        <th>Trạng thái</th>
                        <th>Hot</th>
                        <th>Ngày tạo</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($articles))
                     @foreach($articles as $article)
                     <tr>
                        <td>{{$article -> id}}</td>
                        <td>
                           {{$article -> a_name}}
                        </td>
                        <td>
                           <img src="{{asset("")}}/{{ pare_url_file($article->a_avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        <td>{{$article -> a_description}}</td>
                        <td>
                           <a href="{{route('admin.get.action.article',['active',$article->id])}}" class="badge {{$article->getStatus($article->a_active)['class']}}">{{$article ->getStatus($article->a_active)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.article',['hot',$article->id])}}" class="badge {{$article->getHot($article->a_active)['class']}}">{{$article ->getHot($article->a_active)['name']}}</a>
                        </td>
                        <td>
                           {{$article->created_at}}
                        </td>
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.article',$article->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.article',['delete',$article->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection
@extends('admin::layouts.master')

@section('content')
 <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.list.category')}}">Danh mục/</a><a href="{{route('admin.get.create.category')}}">Thêm mới</a></h1>
                    </div>
                </div>
            </div>
        </div>

	 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Quản lý danh mục <a href="{{route('admin.get.create.category')}}" class="pull-right badge badge-primary"><i class="fas fa-plus-circle"></i> Thêm mới</a></strong>

                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tên danh mục</th>
                        <th>Title Seo</th>
                        <th>Trạng thái</th>
                        <th>TT</th>
                        <th>Mô tả</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(isset($categories))
                      @foreach($categories as $category)
                      <tr>
                        <td>{{$category -> id}}</td>
                        <td>{{$category -> c_name}}</td>
                        <td>{{$category -> c_title_seo}}</td>
                        <td>
                          <a href="{{route('admin.get.action.category',['active',$category->id])}}" class="badge {{$category->getStatus($category->a_active)['class']}}">{{$category ->getStatus($category->a_active)['name']}}</a>
                        </td>
                        <td>
                          <a href="{{route('admin.get.action.category',['hot',$category->id])}}" class="badge {{$category->getHome($category->a_home)['class']}}">{{$category ->getHome($category->a_home)['name']}}</a>
                        </td>
                        <td>{{$category -> c_description_seo}}</td>
                        <td>
                         <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.category',$category->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.category',['delete',$category->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
@endsection
@extends('admin::layouts.master')
@section('content')
{{-- <div class="card-header"> --}}
<div class="breadcrumbs">
   <div class="col-sm-8">
      <div class="page-header float-left">
         <div class="page-title menu">
            
            <h1 style="color: blue;"><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.list.category')}}">Danh mục/</a><a href="{{route('admin.get.create.category')}}">Cập nhật</a></h1>
         
         </div>
      </div>
   </div>
   {{-- <li><a href="">Trang chủ</a></li>
   <li><a href="">Danh mục</a></li>
   <li><a href="">Thêm mới</a></li> --}}

</div>
{{-- </div> --}}
<div class="card">
   <div class="card-header">

      <strong>Cập nhật</strong> 
   </div>
   <div class="card-body card-block">
      @include("admin::category.form")
   </div>
   {{-- 
   <div class="card-footer">
   </div>
</div>
--}}
@endsection
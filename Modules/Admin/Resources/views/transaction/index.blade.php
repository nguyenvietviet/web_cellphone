@extends('admin::layouts.master')
@section('content')



<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.list.category')}}">Đơn hàng/</a><a href="{{route('admin.get.create.category')}}">Danh sách</a></h1>
         </div>
      </div>
   </div>
</div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Quản lý đơn hàng</strong>
            </div>
            <div class="card-body">
               <table id="bootstrap-data-table" class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Tên khách hàng</th>
                        <th>Địa chỉ</th>
                        <th>Số ĐT</th>
                        <th>Tổng tiền</th>
                        <th>Trạng thái</th>
                        <th>Time</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($transactions as $transaction)
                     <tr>
                        <td>{{$transaction->id}}</td>
                        <td>{{isset($transaction->user->name) ? $transaction->user->name : '[N\A]'}}</td>
                        <td>{{$transaction->tr_address}}</td>
                        <td>{{$transaction->tr_phone}}</td>
                        <td>{{number_format($transaction->tr_total,0,',','.')}}VNĐ</td>
                        <td>
                           @if($transaction->tr_status == 1)
                           <a href="#" class="badge badge-success">Đã xử lý</a>
                           @else
                           <a href="{{route('admin.get.active.transaction',$transaction->id)}}" class="badge badge-secondary">Chờ xử lý</a>
                           @endif
                        </td>
                        <td>{{$transaction->created_at->format('d-m-Y')}}</td>
                        <td>
                           <a class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href="{{route('deleteTran',$transaction->id)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                           <button class="badge badge-info js_order_item" data-toggle="modal" data-target ="#myModalOrder" data-id="{{$transaction->id}}" style="padding:5px 10px;border:1px solid #eee;" data-href="{{route('admin.get.view.order',$transaction->id)}}"><i class="fas fa-eye"></i> Xem</button>
                           
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
<!-- Modal -->
<div class="modal hide fade" id="myModalOrder" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Chi tiết đơn hàng #<b class="transaction_id"></b></h4>
         </div>
         <div class="modal-body" id="md_content">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@stop
@section('js')
<script>

$(document).ready(function() {
  $('#myModalOrder').on('show.bs.modal', function(e) {
    let url = $(e.relatedTarget).data('href');
    let transaction_id = $(e.relatedTarget).data('id');

    $.ajax({
      url : url,
      dataType : 'JSON',
      type : 'GET',
      data : {
        id : transaction_id
      },
      success : function(result) {
        console.log(result)
        $("#md_content").html(result);
      }
    });
  })
});

</script>
@endsection
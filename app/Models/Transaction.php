<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Transaction extends Model
{
    //
     protected $table = 'transactions';

    protected $guarded = ['*'];

    const STATUS_DONE =1;
    const STATUS_DEFAULT =0;

    public function user()
    {
    	return $this->belongSto(User::class,'tr_user_id');
    }
}

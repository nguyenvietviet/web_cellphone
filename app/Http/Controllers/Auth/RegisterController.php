<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
use Mail;

class RegisterController extends Controller
{
    public function getRegister()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;

        $user->save();

        if($user->id)
        {
            $email = $user->email;

            $code = bcrypt(md5(time().$email));
            $url = route('user.verify.account',['id' =>$user->id,'code'=>$code]);

            $user->code_active = $code;
            $user->time_active = Carbon::now();
            $user->save();

            $data = [
            'route'=>$url
           ];

           Mail::send('email.verify_account',$data, function($message) use ($email){
                $message->to($email, 'Xác nhận tài khoản')->subject('Xác nhận tài khoản');
            });

                return redirect()->route('get.login');
            }
            return redirect()->back();
    }

    //Xác nhận tài khoản
    public function verifyAccount(Request $request)
    {
        $code = $request->code;
        $id = $request->id;

        $checkUser = User::where([
            'code_active'=>$code,
            'id' =>$id
        ])->first();
        // dd($checkUser);
         if(!$checkUser)
        {
            return redirect('/')->with('warning','Link bị lỗi, vui lòng thử lại sau');
        }
        $checkUser->active =2;
        $checkUser->save();
        // return view('auth.passwords.reset');
        return redirect('/home')->with('thongbao','Xác nhận tài khoản thành công');
    }
}

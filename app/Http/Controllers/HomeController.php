<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Article; 
use App\Models\Category;
use App\Models\Transaction;
use App\Models\Order;
use App\Models\Slide;

class HomeController extends FrontendController
{
    
    public function __construct()
    {
        parent::__construct();
    }

  
    public function index()
    {
        $slide = Slide::all();

    	$productHot = Product::where([
    		'pro_hot' => Product::HOT_ON,
    		'pro_active' => Product::STATUS_PUBLIC
    	])->limit(8)->get();

        $articleNews = Article::orderBy('id','DESC')->limit(6)->get();

        //Hiển thị danh mục sản phẩm nổi bật ra trang chủ
        $categoriesHome = Category::with('products')
        ->where('c_home',Category::HOME)
        ->limit(3)
        ->get();


        
         $productSuggest =[];
        //kiểm tra người dùng đăng nhập
        if(get_data_user('web'))
        {
            $transactions = Transaction::where([
                'tr_user_id'=> get_data_user('web'),
                'tr_status' => Transaction::STATUS_DONE
            ])->pluck('id');


            if(!empty($transactions))
            {
                $listId = Order::whereIn('or_transaction_id',$transactions)->distinct()->pluck('or_product_id');
                // dd($listId);

                if(!empty($listId))
                {
                    $listIdCategory = Product::whereIn('id',$listId)->distinct()->pluck('pro_category_id'); 
                    
                    if($listIdCategory)
                    {
                        $productSuggest = Product::whereIn('pro_category_id',$listIdCategory)->limit(3)->get();
                    }
                } 
            }
        }

        $viewData = [
            // '$productPublic'=>$productPublic,
        	'productHot' =>$productHot,
            'slide' =>$slide,
            'articleNews' =>$articleNews,
            'categoriesHome' =>$categoriesHome,
            'productSuggest' =>$productSuggest
        ];
        return view('home.index',$viewData);
    }
}

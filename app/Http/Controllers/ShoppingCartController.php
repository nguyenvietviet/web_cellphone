<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Transaction;
use Carbon\Carbon;

class ShoppingCartController extends FrontendController
{
    public function __construct(){
      parent::__construct();
    }
    /* Add shopingCart*/
    public function addProduct(Request $request,$id)
    {
    	$product = Product::select('pro_name','id','pro_price','pro_sale','pro_avatar','pro_number')->find($id);

    	if(!$product) return redirect('/home');

        $price = $product->pro_price;

        if($product->pro_sale)
        {
            $price = $price *(100 - $product->pro_sale)/100;
        }

        if($product->pro_number == 0)
        {
            return redirect()->back()->with('warning','sản phẩm đã hết hàng');
        }
    	\Cart::add([
    		'id' => $id, 
    		'name' => $product->pro_name, 
    		'qty' => 1, 
    		'weight' => 1,
    		'price' => $price, 
    		'options' => [
                'avatar' => $product->pro_avatar,
                'sale' => $product->pro_sale,
                'price_old' =>$product->pro_price
            ]
    	]);
    	return redirect()->back();
    }
    /* xóa giỏ hàng */
    public function deleteProductItem($key)
    {
        \Cart::remove($key);
        return redirect()->back()->with('thongbao','Xóa thành công');
    }

    /* Danh sách giỏ hàng*/
    public function getListShoppingCart()
    {
    	$products = \Cart::content();
    	return view('shopping.index',compact('products'));
    }

    /* Thanh toán */

    public function getFormPay()
    {
        $products = \Cart::content();
        return view('shopping.pay',compact('products'));
    }

    /* Lưu thông tin giỏ hàng*/
    public function saveInforShoppingCart(Request $request)
    {
        $totalMoney = str_replace(',','',\Cart::subtotal(0,3));
        // dd($request->all());
        $transactionId = Transaction::insertGetId([
            'tr_user_id' => get_data_user('web'),
            'tr_total' => (int)$totalMoney,
            'tr_note' => $request ->note,
            'tr_phone' => $request ->phone,
            'tr_address' => $request ->address,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        if($transactionId)
        {
            $products = \Cart::content();
            foreach ($products as $product) {
                Order :: insert([
                    'or_transaction_id' => $transactionId,
                    'or_product_id' => $product->id,
                    'or_qty' => $product->qty,
                    'or_price' => $product->options->price_old,
                    'or_sale' => $product->options->sale
                ]);
            }
        }
        \Cart::destroy();
        return redirect('\home')->with('thongbao','Đã gửi thông tin thanh toán');
       
    }
}

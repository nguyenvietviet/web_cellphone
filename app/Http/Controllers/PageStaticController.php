<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PageStatic;

class PageStaticController extends FrontendController
{
	 public function __construct(){
      parent::__construct();
    }
    //
    public function aboutUs()
    {
    	$page = PageStatic::where('ps_type',PageStatic::TYPE_ABOUT)->first();
    	return view('page_static.about',compact('page'));
    }
    public function aboutThongtin()
    {
    	$page = PageStatic::where('ps_type',PageStatic::TYPE_INFO_SHOPPING)->first();
    	return view('page_static.thongtin',compact('page'));
    }
    public function aboutBaomat()
    {
    	$page = PageStatic::where('ps_type',PageStatic::TYPE_BAOMAT)->first();
    	return view('page_static.baomat',compact('page'));
    }
    public function aboutDieukhoan()
    {
    	$page = PageStatic::where('ps_type',PageStatic::TYPE_DIEUKHOAN)->first();
    	return view('page_static.dieukhoan',compact('page'));
    }
}

@extends('layouts.app')
@section('content')
<style type="text/css">
	.article_content h1{
		font-size: 1.4rem;
		font-weight: bold;
	}
	.ariticle_content {
		font-family: Roboto,sans-serif;
	}
	
</style>
<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="container-inner">
							<ul>
								<li class="home">
									<a href="{{route('home')}}">Trang chủ</a>
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li class="home">
									<a href="{{route('get.list.article')}}">bài viết</a>
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li class="category3"><span>{{$articleDetail->a_name}}</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="main-contact-area">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="article_content" style="margin-bottom: 20px;">
							<h2>{{$articleDetail->a_name}}</h2>
							<p style="font-weight: 400;color: #333;">{{$articleDetail->a_description}}</p>
							<div>
								{!!$articleDetail->a_content!!}
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<hr>
						<h4 style="font-weight: bold; margin-bottom: 2%;">Bài viết khác</h4>
						<hr>
						@include('components.article')
					</div>
					<div class="ol-md-3">
						<h4>Bài viết nổi bật</h4>
						<div class="list_article_hot">

							@include('components.article_hot')

						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
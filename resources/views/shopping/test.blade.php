<div class="main-contact-area">
			<div class="container wrapper">
            <div class="row cart-body">
                <form class="form-horizontal" method="post" action="">
                	@csrf
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                    <!--REVIEW ORDER-->
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Danh sách sản phẩm <div class="pull-right"><small><a class="afix-1" href="{{route('get.list.shopping.cart')}}">Cập nhật</a></small></div>
                        </div>
                        @foreach($products as $product)
                        
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-3 col-xs-3">
                                    <img class="img-responsive" style="width: 70px; height: 70px;" src="{{asset("")}}/{{pare_url_file($product->options->avatar)}}" />
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <div class="col-xs-12">{{$product->name}}</div>
                                    <div class="col-xs-12"><small>Số lượng: x<span>{{$product->qty}}</span></small></div>
                                </div>
                                <div class="col-sm-3 col-xs-3 text-right">
                                    <h6>{{number_format($product->price,0,',','.')}} VND</h6>
                                </div>
                            </div>
                            
  
                        @endforeach
                         <div class="form-group"><hr /></div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <strong>Tổng tiền</strong>
                                    <div class="pull-right"><span>{{\Cart::subtotal()}} VNG</span></div>
                                </div>
                            </div>
                            <div class="form-group"><hr /></div>
                         </div>  
						</div>
                    <!--REVIEW ORDER END-->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6">
                    <!--SHIPPING METHOD-->
                    <div class="panel panel-info">
                        <div class="panel-heading">Thông tin thanh toán</div>
                        <div class="panel-body">
                        
                            <div class="form-group">
                                <div class="col-md-12"><strong>Địa chỉ:</strong></div>
                                <div class="col-md-12">
                                    <input type="text" name="address" class="form-control" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><strong>Email:</strong></div>
                                <div class="col-md-12">
                                    <input type="text" name="email" class="form-control" value="{{get_data_user('web','email')}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><strong>Số ĐT:</strong></div>
                                <div class="col-md-12">
                                    <input type="text" name="phone" class="form-control" value="{{get_data_user('web','phone')}}" />
                                </div>
                            </div>
                            	
                            <div class="form-group">
                                <div class="col-md-12"><strong>Ghi chú:</strong></div>
                                <div class="col-md-12"><textarea type="text" name="note" rows="5" cols="40" class="form-control" value=""></textarea></div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-12">
                            		<button type="submit" class="btn btn-success">Xác nhận thông tin</button>
                            	</div>
                            </div>
                        </div>
                    </div>
                    <!--SHIPPING METHOD END-->
                </div>
                
                </form>
            </div>
            <div class="row cart-footer">
        	
            </div>
    </div>
</div>
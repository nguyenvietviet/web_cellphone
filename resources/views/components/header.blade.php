     <header class="header" style="line-height: 40px; background-color: black;color:white;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">Hotline:0363701249</div>
                    <div class="col-md-6 text-right"> Hỗ trợ 24/7</div>

                </div>
            </div>
        </header>
     <header class="short-stor"  style=" box-shadow: 0 4px 2px -2px #dfdfdf;" >



            <div class="container">
                <div class="row">
                    
                    <div class="col-md-3 col-sm-12 text-center nopadding-right">
                        <div class="top-logo">
                            <a href="{{route('home')}}"><img src="img/logo.jpg" style="width: 130px;height: 40px;" alt="" /></a>
                        </div>
                    </div>
                  
                    <div class="col-md-6 text-center">
                        <div class="mainmenu" >
                            <nav>
                                <ul>
                                    <li class="expand"><a href="{{route('home')}}">Trang chủ</a></li>
                                    <li class="expand">
                                        <a>Sản phẩm</a>
                                        <ul class="restrain sub-menu">
                                        @if(isset($categories))
                                            @foreach($categories as $cate)
                                            <li><a href="{{route('get.list.product',[$cate->c_slug,$cate->id])}}">{{$cate->c_name}}</a></li>                   
                                            @endforeach
                                        @endif
                                    </ul>
                                    </li>
                                    <li class="expand"><a href="{{route('get.list.article')}}">Tin tức</a></li>
                                    <li class="expand"><a href="{{route('get.about_us')}}">Giới thiệu</a>
                                        <ul class="restrain sub-menu">
                                            <li><a href="{{route('get.about_us')}}">Về chúng tôi</a></li>
                                            <li><a href="{{route('get.thongtin')}}">Thông tin giao hàng</a></li>
                                            <li><a href="{{route('get.baomat')}}">Chính sách bảo mật</a></li>
                                            <li><a href="{{route('get.dieukhoan')}}">Điều khoản sử dụng</a></li>
                                        </ul>
                                    </li>
                                    <li class="expand"><a href="{{route('get.contact')}}">Liên hệ</a></li>
                              
                                  </ul>
                            </nav>
                        </div>
                       
                        <div class="row">
                            <div class="col-sm-12 mobile-menu-area">
                                <div class="mobile-menu hidden-md hidden-lg" id="mob-menu">
                                    <span class="mobile-menu-title">Menu</span>
                                    <nav>
                                        <ul>
                                            <li><a href="index.html">Trang chủ</a>
                                               
                                            </li>
                                            <li><a href="shop-grid.html">Sản phẩm</a>
                                                <ul>
                                                    @if(isset($categories))
                                                        @foreach($categories as $cate)
                                                        <li><a href="{{route('get.list.product',[$cate->c_slug,$cate->id])}}">{{$cate->c_name}}</a></li>                                                          @endforeach
                                                    @endif
                                                    
                                                </ul>                                          
                                            </li>
                                           <li><a href="{{route('get.list.article')}}">Tin tức</a></li>
                                           <li><a href="index.html">Giới thiệu</a></li>
                                           <li><a href="index.html">Liên hệ</a></li>
                                        </ul>
                                    </nav>
                                </div>                      
                            </div>
                        </div>
                        
                    </div>
                   
                    <div class="col-md-3 col-sm-12 nopadding-left">
                        <div class="top-detail">
                         
                            
                            <div class="disflow">
                                <div class="circle-shopping expand">
                                    <div class="shopping-carts text-right">
                                        <div class="cart-toggler">
                                            <a href="{{route('get.list.shopping.cart')}}"><i class="icon-bag"></i></a>
                                            <a href="{{route('get.list.shopping.cart')}}"><span class="cart-quantity">{{Cart::count()}}</span></a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            
                            <div class="disflow">
                                <div class="header-search expand">
                                    <div class="search-icon fa fa-search"></div>
                                    <div class="product-search restrain">
                                        <div class="container nopadding-right">
                                            <form action="index.html" id="searchform" method="get">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" maxlength="128" placeholder="Search product...">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- search divition end -->
                            <div class="disflow">
                                <div class="expand dropps-menu">
                                    <a href="{{route('get.login')}}"><i class="fa fa-align-right"></i></a>
                                    <ul class="restrain language">
                                        @if(Auth::check())
                                        <li><a href="{{route('user.dashboard')}}">Quản lý</a></li>
                                        <li><a href="wishlist.html">Sản phẩm yêu thích</a></li>
                                        <li><a href="{{route('get.list.shopping.cart')}}">Giỏ hàng</a></li>
                                        <li><a href="{{route('get.logout.user')}}">Thoát</a></li>
                                        @else
                                            <li><a href="{{route('get.register')}}">Đăng ký</a></li>
                                            <li><a href="{{route('get.login')}}">Đăng nhập</a></li>
                                        @endif
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </header> 
        
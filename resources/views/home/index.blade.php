@extends('layouts.app')
@section('content')
<style type="text/css">
   .active {
   color:#ff9705;
   }
</style>
@include('components.slide')
<!-- product section start -->
@if(isset($productHot))
<div class="our-product-area new-product">
   <div class="container" id="myDIV">
      <div class="area-title">
         <h2> Sản phẩm nổi bật</h2>
      </div>
      <!-- our-product area start -->
      <div class="row">
         <div class="col-md-12">
            <div class="row" style="border: 1px solid #ccc!important;border-radius: 7px;">
             
                  @foreach($productHot as $proHot)

                  <div class="col-md-4">
                     <div class="single-product first-sale" >
                        <div class="product-img">
                           @if($proHot->pro_number== 0)
                           <span style="background-color: #e91e63;position: absolute;color: white;padding:2px 6px; border-radius: 5px;font-size: 12px;">Tạm hết hàng</span>
                           @endif
                           @if($proHot->pro_sale)
                           <span style="position: absolute;background-image: linear-gradient(-90deg,#ec1f1f 0%,#ff9c00 100%);border-radius: 10px;padding: 1px 7px;color: white;font-size: 10px;right: 0">Khuyễn mãi {{$proHot->pro_sale}}%</span>
                           @endif
                           <a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}">
                           <img class="primary-image" src="{{asset("")}}/{{ pare_url_file($proHot->pro_avatar)}}" alt="" />
                           <img class="secondary-image" src="{{asset("")}}/{{ pare_url_file($proHot->pro_avatar)}}" alt="" />
                           </a>
                           <div class="tra-gop"><span>Trả góp 0%</span></div>
                           {{--  
                           <div class="product_type product_type_hot product_type_order_0">Hot</div>
                           --}}
                           <div class="action-zoom">
                              <div class="add-to-cart">
                                 <a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}" title="Xem chi tiết"><i class="fa fa-search-plus"></i></a>
                              </div>
                           </div>
                           <div class="actions">
                              <div class="action-buttons">
                                 <div class="add-to-links">
                                   {{--  <div class="add-to-wishlist">
                                       <a href="#" title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                    </div> --}}
                                    <div class="compare-button">
                                       <a href="{{route('add.shopping.cart',$proHot->id)}}" title="Thêm vào giỏ hàng"><i class="fas fa-cart-plus"></i></a>
                                    </div>
                                 </div>
                               
                              </div>
                           </div>
                           <div class="price-box">
                              <span class="new-price">{{number_format($proHot->pro_price,0,',','.')}}đ</span>
                           </div>
                        </div>
                        <div class="product-content">
                           <h2 class="product-name"><a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}">{{$proHot->pro_name}}</a></h2>
                           <h5 class="price" style="color:#f57224;">{{number_format($proHot->pro_price,0,',','.')}}đ</h5>
                           <p class="shottext">{{$proHot->pro_description}}</p>
                           <a href="{{route('add.shopping.cart',$proHot->id)}}" class="form-control btn btn-primary">Đặt mua</a>
                        </div>
                     </div>
                  </div>
                  @endforeach
                  <!-- single-product end -->
                
            </div>
         </div>
      </div>
   </div>
</div>
<!-- our-product area end --> 
<!--TTTTTTTTTTTTTTTTTTTTTTTTTTT-->
@endif
<!-- latestpost area start -->
@if(isset($articleNews))
<div class="latest-post-area">
   <div class="container">
      <div class="area-title">
         <h2>Bài viết mới</h2>
      </div>
      <div class="row" style="border: 1px solid #ccc!important;border-radius: 7px;">
         <div class="all-singlepost">
            <!-- single latestpost start -->
            @foreach($articleNews as $arNew)
            <div class="col-md-4 col-sm-4">
               <div class="single-post" style="margin-bottom: 40px;">
                  <div class="post-thumb">
                     <a href="{{route('get.detail.article',[$arNew->a_slug,$arNew->id])}}">
                     <img src="{{asset("")}}/{{ pare_url_file($arNew->a_avatar)}}" alt="" style="width: 100%;height: 330px; object-fit: cover;" />
                     </a>
                  </div>
                  <div class="post-thumb-info">
                     <div class="postexcerpt">
                        <p style="height:50px;">{{$arNew->a_name}}</p>
                        <a href="{{route('get.detail.article',[$arNew->a_slug,$arNew->id])}}" class="read-more">Xem thêm</a>
                     </div>
                  </div>
               </div>
            </div>
            @endforeach
            <!-- single latestpost end -->
         </div>
      </div>
   </div>
</div>
@endif
@include('components.product_view')
<!-- latestpost area end -->
<!-- block category area start -->
<div class="block-category">
   <div class="container">
      <div class="row">
         @if(isset($categoriesHome))
         <!-- featured block start -->
         @foreach($categoriesHome as $categoriHome)
         <div class="col-md-4">
            <!-- block title start -->
            <div class="block-title">
               <h2 style="border-radius: 7px;">{{$categoriHome->c_name}}</h2>
            </div>
            @if(isset($categoriHome->products))
            <div class="block-carousel">
               @foreach($categoriHome->products as $product)
               <?php 
                  $ageDetail =0;
                  if($product->pro_total_rating)
                  {
                     $ageDetail = round($product->pro_total_number/$product->pro_total_rating,2);
                  }
                  ?>
            <!--Tạo biến Price để tính giảm giá-->
                <?php 
                 $price = $product->pro_price;

                    if($product->pro_sale)
                    {
                        $price = $price *(100 - $product->pro_sale)/100;
                 }
                  ?>
             <!--Tạo biến Price để tính giảm giá-->

               <div class="block-content">
                  <!-- single block start -->
                  <div class="single-block">
                     <div class="block-image pull-left">
                        <a href="{{route('get.detail.product',[$product->pro_name,$product->id])}}"><img src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" style="width: 170px;height: 208px;" alt="" /></a>
                     </div>
                     <div class="category-info">
                        <h3><a href="{{route('get.detail.product',[$product->pro_name,$product->id])}}">{{$product->pro_name}}</a></h3>
                        <p>{{$product->depscription}}</p>

                        <div class="cat-price">{{$price}} <span class="old-price">{{number_format($product->pro_price,0,',','.')}}</span></div>
                        <div class="cat-rating">
                           @for($i=1; $i<=5; $i++)
                           <a><i class="fa fa-star {{$i <= $ageDetail ? 'active' : ''}}"></i></a>
                           @endfor
                        </div>
                     </div>
                  </div>
                  <!-- single block end -->
               </div>
               @endforeach
            </div>
            @endif
         </div>
         @endforeach
         @endif
      </div>
   </div>
</div>
<!-- block category area end -->
<div class="testimonial-area lap-ruffel">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="crusial-carousel">
               <div class="crusial-content">
                  <p>“Mạnh mẽ, bền bỉ, tin cậy"</p>
                  <span>Việt Mobile</span>
               </div>
               <div class="crusial-content">
                  <p>“Chất lượng - Uy tín - Chính hãng"</p>
                  <span>Việt Mobile</span>
               </div>
               <div class="crusial-content">
                  <p>“Nếu những gì mà chúng tôi không có ,nghĩa là bạn không cần"</p>
                  <span>Việt Mobile</span>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop